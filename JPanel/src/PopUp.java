import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.text.DecimalFormat;

public class PopUp extends JFrame implements ActionListener{
	private JLabel lbl1, lbl2, lbl3, lblScore, lblWeight, lblOne,lblTwo, lblThree, lblFour, lblAve;
	private JTextField tf1, tf2, tf3, tf4, tfOne, tfTwo, tfThree, tfFour, tfAve;
	private JButton button;
	private JPanel pnlWeight, pnlOne, pnlTwo, pnlThree, pnlFour, pnlAve;
	private static final int WIDTH = 600;
	private static final int HEIGHT = 400;
	
	public PopUp() {
		lbl1 = new JLabel("");
		lbl2 = new JLabel("");
		lbl3 = new JLabel("");
		lblScore = new JLabel("Score", SwingConstants.CENTER);
		
		
		lblWeight = new JLabel();
		pnlWeight = new JPanel();
		lblWeight.setText("Weight");
		pnlWeight.add(lblWeight, BorderLayout.WEST);
		
		lblOne = new JLabel();
		pnlOne = new JPanel();
		lblOne.setText("Test One:");
		pnlOne.add(lblOne, BorderLayout.WEST);

		lblTwo = new JLabel();
		pnlTwo = new JPanel();
		lblTwo.setText("Test Two:");
		pnlTwo.add(lblTwo, BorderLayout.WEST);
		
		lblThree = new JLabel();
		pnlThree = new JPanel();
		lblThree.setText("Test Three:");
		pnlThree.add(lblThree, BorderLayout.WEST);
		
		lblFour = new JLabel();
		pnlFour = new JPanel();
		lblFour.setText("Test Four:");
		pnlFour.add(lblFour, BorderLayout.WEST);
		
		lblAve = new JLabel();
		pnlAve = new JPanel();
		lblAve.setText("Test Ave:");
		pnlAve.add(lblAve, BorderLayout.WEST);
	
		
		tf1 = new JTextField();
		tf2 = new JTextField();
		tf3 = new JTextField();
		tf4 = new JTextField();
		tfOne = new JTextField();
		tfTwo = new JTextField();
		tfThree = new JTextField();
		tfFour = new JTextField();
		tfAve = new JTextField();
		
		//Creates calculation button
		button = new JButton("Calculate");
		button.addActionListener(this);
		
		//Creates and describes the pop-up panel
		setTitle("Score Calculator");
		setSize(WIDTH, HEIGHT);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		Container Pane = getContentPane();
		Pane.setLayout(new GridLayout(7,3));
		
		//Titles each column Score and Weight
		Pane.add(lbl1);
		Pane.add(lblScore);
		Pane.add(lblWeight);
		
		//Test and Weight 1
		Pane.add(lblOne);
		Pane.add(tf1);
		Pane.add(tfOne);
		
		//Test and Weight 2
		Pane.add(lblTwo);
		Pane.add(tf2);
		Pane.add(tfTwo);
		
		//Test and Weight 3
		Pane.add(lblThree);
		Pane.add(tf3);
		Pane.add(tfThree);
		
		//Test and Weight 4
		Pane.add(lblFour);
		Pane.add(tf4);
		Pane.add(tfFour);
		
		//Shows Test and Weighted Average
		Pane.add(lblAve);
		Pane.add(tfAve);
		Pane.add(lbl2);
		
		//Calculation Button
		Pane.add(button);
		Pane.add(lbl3);
		
	}
	
	public void actionPerformed(ActionEvent e){
            double one, wone, two, wtwo, three, wthree, four, wfour;
            DecimalFormat twoDigits = new DecimalFormat("0.00");
            one = Double.parseDouble(tf1.getText());
            wone = Double.parseDouble(tfOne.getText());
            two = Double.parseDouble(tf2.getText());
            wtwo = Double.parseDouble(tfTwo.getText());
            three = Double.parseDouble(tf3.getText());
            wthree = Double.parseDouble(tfThree.getText());
            four = Double.parseDouble(tf4.getText());
            wfour = Double.parseDouble(tfFour.getText());
            //Use of Array
            double Average = ((one * wone) + (two * wtwo) + (three * wthree) + (four * wfour))/(wone+wtwo+wthree+wfour);
            tfAve.setText(""+ twoDigits.format(Average));
        }

    
	public static void main(String[] args) {
		new PopUp();
	}

	}
	


